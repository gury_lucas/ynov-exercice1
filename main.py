from bottle import route, run, template
from misc import add


@route('/hello/<name>')
def index(name):
    return template('<b>Hello {{name}}</b> !', name=name)


@route('/add/<a>/<b>')
@route('/add/<a>/<b>/')
def add_index(a, b):
    return add(a, b)


run(host='localhost', port=8080, reloader=True)
