def add(a, b):
    """
    Cette fonction retourne la somme de deux nombres en json.
    Exemples :
    >>> add(2, 3)
    "{'result'}: 5"
    """
    Val = int(a) + int(b)
    return "{'result'}: " + str(Val)
